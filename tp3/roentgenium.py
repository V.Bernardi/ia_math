# 111

# CONVOLUTION

import numpy as np
from skimage import data
import matplotlib as plt
from scipy import signal
from matplotlib.pyplot import imshow, get_cmap
import matplotlib.pyplot as plt

def displayTwoBaWImages(img1, img2):
    _, axes = plt.subplots(ncols=2)
    axes[0].imshow(img1, cmap=plt.get_cmap('gray'))
    axes[1].imshow(img2, cmap=plt.get_cmap('gray'))
    plt.show()

creeper = np.array([
    [0,0,0,0,0],
    [0,1,0,1,0],
    [0,0,1,0,0],
    [0,1,1,1,0],
    [0,1,0,1,0]
])
cmp = get_cmap('gray')
# plt.imshow(creeper, cmap=cmp, vmin=0, vmax=1)

cible = np.array([
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0.5,0.5,0,0,0],
    [0,0,0,0.5,0.5,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
])
# plt.imshow(cible, cmap=cmp, vmin=0, vmax=1)
# plt.show()


# matrice de convolution => comme un filtre
kernel = np.ones((3,3), np.float32)/2
# print(kernel)

imgconvol = signal.convolve2d(
    cible,
    kernel,
    mode='same',
    boundary='fill',
    fillvalue=0
)

# print(imgconvol)
# displayTwoBaWImages(cible, imgconvol)


# 4 : convolution à dos de cheval

kernel_cheval =[
    [0,1,0],
    [1,-4,1],
    [0,1,0]
] 

horse_image = data.horse()

im_convol_cheval = signal.convolve2d(
    horse_image,
    kernel_cheval,
    mode='same',
    boundary='fill',
    fillvalue=0
)

# displayTwoBaWImages(horse_image, im_convol_cheval)



# 5 : selfie avec la convolution 

camera_image = data.camera()

# on re-normalise l'image 
image_normalized = (camera_image - np.min(camera_image)) / (np.max(camera_image) - np.min(camera_image))

kernel_camera = [
    [0,-1,0,],
    [-1,5,-1],
    [0,-1,0]
]

im_convol_camera = signal.convolve2d(
    camera_image,
    kernel_camera,
    mode='same',
    boundary='fill',
    fillvalue=0
)

# displayTwoBaWImages(camera_image, im_convol_camera)


# 6 : nouveau filtre, nouveaux horizons

checkerboard_image=data.checkerboard()

kernel_checkerboard = [
    [1,1,1],
    [1,1,1],
    [1,1,1]
]/9

im_convol_cherckerboard = signal.convolve2d(
    checkerboard_image,
    kernel_checkerboard,
    mode='same',
    boundary='fill',
    fillvalue=0
)

displayTwoBaWImages(checkerboard_image, im_convol_cherckerboard)