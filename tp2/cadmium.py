# 48
import matplotlib.pyplot as plt
from math import log


def entropie(p,q):
    somme = 0
    for i in range(len(p)):
        somme += p[i]*log(q[i])
    return -somme


p = [0,0,0,1]
# 1
# q = [0.25, 0.25, 0.25, 0.25]
# 2
# q = [0.01, 0.01 , 0.49, 0.49]
# 3
q = [0.9, 0.03, 0.03, 0.04]
# print(entropie(p, q))



import numpy as np
from tensorflow import keras
from tensorflow.keras import optimizers
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense

### Partie A - Les données
from tensorflow.keras.datasets import mnist
from tensorflow.keras.utils import to_categorical

# Téléchargement des données
(X_train_data,Y_train_data),(X_test_data,Y_test_data) = mnist.load_data()
N = X_train_data.shape[0] # N = 60 000 données

# Données d'apprentissage X
X_train = np.reshape(X_train_data,(N,784)) # vecteur image
X_train = X_train/255 # normalisation

# Données d'apprentissage Y vers une liste de taille 10
Y_train = to_categorical(Y_train_data, num_classes=10)

# Données de test
X_test = np.reshape(X_test_data,(X_test_data.shape[0],784))
X_test = X_test/255
Y_test = to_categorical(Y_test_data, num_classes=10)


### Partie B - Le réseau de neurones
p = 8
modele = Sequential()

# Première couche : p neurones (entrée de dimension 784 = 28x28)
modele.add(Dense(p, input_dim=784, activation='sigmoid'))

# Deuxième couche : p neurones
modele.add(Dense(p, activation='sigmoid'))

# Couche de sortie : 1O neurones (un par chiffre)
modele.add(Dense(10, activation='softmax'))

# La fonction d'activation 'softmax' sera décrite dans le paragraphe suivant.
# Choix de la méthode de descente de gradient
modele.compile(loss='categorical_crossentropy',
    optimizer='sgd',
    metrics=['accuracy'])


# La fonction d'erreur 'categorical_crossentropy' est décrite dans le paragraphe suivant.
# L'optimisation 'sgd' est décrite dans le paragraphe suivant.
# 'accuracy' est décrite dans le paragraphe suivant.
# print(modele.summary)



### Partie C - Calcul des poids par descente de gradient

# batch_size précise la taille de l’échantillon 
    # — une descente de gradient stochastique pure : batch_size=1,
    # — une descente de gradient sur la totalité des N données : batch_size=N,
    # — ou toute valeur intermédiaire : par défaut batch_size=32.
# epochs => nombre d'étapes dans la méthode de gradient
# verbose => afficher détail : 0 rien , 1 barre progression, 2 num epoque
modele.fit(X_train, Y_train, batch_size=32, epochs=40, verbose=0)
# La fonction fit() lance la descente de gradient (avec une initialisation aléatoire des poids). L’option
# batch_size détermine la taille du lot (batch). On indique aussi le nombre d’époques à effectuer (avec
# epochs=40, chacune des N = 60 000 données sera utilisée 40 fois, mais comme chaque étape regroupe un
# lot de 32 données, il y 40 × N /32 étapes de descente de gradient).

### Partie D - Résultats
resultat = modele.evaluate(X_test, Y_test, verbose=0)
print('Valeur de l''erreur sur les données de test (loss):', resultat[0])
print('Précision sur les données de test (accuracy):', resultat[1])
# La fonction evaluate() renvoie la valeur de la fonction d’erreur (qui est la fonction minimisée par la
# descente de gradient mais qui n’a pas de signification tangible). Ici la fonction renvoie aussi la précision
# (car on l’avait demandée en option dans la fonction compile()).



# Prédiction sur les données de test
Y_predict = modele.predict(X_test)

# Un exemple
# 7 2 1 0 4 1 4 9 5 9
i = 1 # numéro de l'image
chiffre_predit = np.argmax(Y_predict[i]) # prédiction par le réseau
print("Sortie réseau", Y_predict[i])
print("Chiffre attendu :", Y_test_data[i])
print("Chiffre prédit :", chiffre_predit)
plt.imshow(X_test_data[i], cmap='Greys')
plt.show()


