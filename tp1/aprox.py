from matplotlib import pyplot as plt
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
import numpy as np

# Partie A. Données
# Fonction à approcher

def f(x):
    return np.cos(2*x) + x*np.sin(3*x) + x**0.5 - 2


a, b = 0, 5 # intervalle [a,b]
N = 100 # taille des données
X = np.linspace(a, b, N) # abscisses
Y = f(X) # ordonnées
X_train = X.reshape(-1,1)
Y_train = Y.reshape(-1,1)
# Partie B. Réseau
modele = Sequential()
p = 10
modele.add(Dense(p, input_dim=1, activation='tanh'))
modele.add(Dense(p, activation='tanh'))
modele.add(Dense(p, activation='tanh'))
modele.add(Dense(p, activation='tanh'))
modele.add(Dense(1, activation='linear'))

# Méthode de gradient : descente de gradient classique
modele.compile(loss='mean_squared_error')
print(modele.summary())
# Partie C. Apprentissage
# On initialise d'abord les poids aléatoirement (car nous voulons faire plusieurs
#essais d'apprentissage en repartant de zero à chaque fois) :
random_weights = [np.random.normal(size=w.shape) for w in modele.get_weights()]
modele.set_weights(random_weights)
# On lance l'apprentissage (par descente de gradient)
history = modele.fit(X_train, Y_train, epochs=4000)
# Partie D. Visualisation
# Affichage de la fonction et de son approximation
Y_predict = modele.predict(X_train)
plt.plot(X_train, Y_train, color='blue')
plt.plot(X_train, Y_predict, color='red')
plt.show()
# Affichage de l'erreur au fil des époques
plt.plot(history.history['loss'])
plt.show()