from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
import numpy as np

# Architecture du réseau
# type sequential => va être décrit couche par couche
modele = Sequential()

# Couches de neurones
# l'ordre des add et important
# Dense( nb_neurone_de_la_couche, nb_entrées, fct_activation )
modele.add(Dense(2, input_dim=1, activation='relu'))
modele.add(Dense(1, activation='relu'))


# Poids

# Couche 0
#                                1ère entrée         2ème ...
# vals de la couche [[1er neurones, 2 ème, etc...], [...], ...]
coeff = np.array([[1.,-0.5]])
biais = np.array([-1,1])
poids = [coeff,biais]
modele.layers[0].set_weights(poids)

# Couche 1
coeff = np.array([[1.0],[1.0]])
biais = np.array([0])
poids = [coeff,biais]
modele.layers[1].set_weights(poids)


# Evaluation

entree = np.array([[3.0]])
sortie = modele.predict(entree)
print("sortie", sortie) # doit donner 2


# Visualisation

import matplotlib.pyplot as plt

liste_x = np.linspace(-2, 3, num=100)

entree = np.array([[x] for x in liste_x])
sortie = modele.predict(entree)

liste_y = np.array([y[0] for y in sortie])

# plt.plot(liste_x,liste_y)
# plt.show()

