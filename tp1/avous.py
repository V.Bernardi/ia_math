from matplotlib import pyplot as plt
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
import numpy as np

modele = Sequential()

# Couches de neurones
# Dense( nb_neurone_de_la_couche, nb_entrées, fct_activation )
modele.add(Dense(3, input_dim=2, activation='sigmoid'))
modele.add(Dense(1, activation='sigmoid'))


# Poids

# Couche 0
#                                1ère entrée         2ème ...
# vals de la couche [[1er neurones, 2 ème, etc...], [...], ...]
coeff = np.array([[1.,3., -5.], [2.,-4.,-6.]])
biais = np.array([-1,0,1])
poids = [coeff,biais]
modele.layers[0].set_weights(poids)

# Couche 1
coeff = np.array([[1.0],[1.0],[1.0]])
biais = np.array([-3])
poids = [coeff,biais]
modele.layers[1].set_weights(poids)



# Resultat
entree = np.array([[1, -1]])
sortie = modele.predict(entree)
print("sortie", sortie)

# F (7,-5) => [[0.12303182]]
# F (1,-1) => [[0.26876235]]



# Visualisation 3D

from mpl_toolkits.mplot3d import Axes3D

VX = np.linspace(-5, 5, 20)
VY = np.linspace(-5, 5, 20)
X,Y = np.meshgrid(VX, VY)

entree = np.c_[X.ravel(), Y.ravel()]
sortie = modele.predict(entree)

Z = sortie.reshape(X.shape)
fig = plt.figure()
ax = plt.axes(projection='3d')
ax.plot_surface(X, Y, Z)

plt.show()